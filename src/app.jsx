import React, { Suspense } from 'react';
import RootRouts from './components/routes/rootRouts';

/**
 * Some documented react component without any props
 *
 * @component
 * @example
 * return (
 *   <App />
 * )
 */
export default function App() {
  return (
    <Suspense fallback="Loading...">
      <RootRouts />
      <div>Hello World</div>
    </Suspense>
  );
}
