import React from 'react';
import { Switch, Route } from 'react-router';
import Example from '../example/example';

/**
 * Root Routs Component
 * @component
 * @example
 * return (
 *   <RootRouts />
 * )
 */
export default function RootRouts() {
  return (
    <Switch>
      <Route exact path="/" component={Example} />
      <Route render={() => '404'} />
    </Switch>
  );
}
