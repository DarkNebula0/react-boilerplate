import React from 'react';
import Example from '../../../src/components/example/example';

export default { title: 'components/Example' };

export const simple = () => <Example version="0.0.1" date={new Date()} />;
