import React from 'react';
import { StoryProvider } from '../../utils/storyProvider';
import RootRouts from '../../../src/components/routes/rootRouts';

export default { title: 'components/Routes' };

export const basic = () => <RootRouts />;
