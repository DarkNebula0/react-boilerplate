import React, { Suspense } from 'react';
import { createStore, compose } from 'redux';
import { Provider } from 'react-redux';
import rootReducer from '../../src/redux/rootReducer';
import { BrowserRouter } from 'react-router-dom';

const storeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// Initialize store
const store = createStore(rootReducer, storeEnhancers);

export function StoryProvider({ children }) {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Suspense fallback="Loading...">{children}</Suspense>
      </BrowserRouter>
    </Provider>
  );
}
