/* eslint-disable import/no-extraneous-dependencies */
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = {
  entry: {
    bundle: './index.js',
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader', 'eslint-loader'],
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              url: (url) => !url.startsWith('/'),
            },
          },
          'sass-loader',
        ],
      },
      {
        exclude: /node_modules/,
        test: /\.(png|jpg|svg)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'images/[folder]/[name].[ext]',
              ignore: '/node_modules/',
            },
          },
        ],
      },
      {
        test: /\.(ttf|woff2|woff|eot|svg)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'fonts/[name].[ext]',
            },
          },
        ],
      },
      {
        test: /\.json$/,
        type: 'javascript/auto',
        exclude: /(node_modules|bower_components)/,
        exclude: /src/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'locales/[folder]/[name].[ext]',
            },
          },
        ],
      },
    ],
  },
  resolve: {
    extensions: ['*', '.js', '.jsx'],
  },
  output: {
    path: path.join(__dirname, '/../dist'),
    publicPath: '/',
    filename: '[name]-[hash].js',
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'styles-[hash].css',
    }),
    new HtmlWebpackPlugin({
      template: 'src/index.html',
    }),
  ],
};
